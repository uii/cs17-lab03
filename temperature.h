#pragma once

#include <sstream>

enum Scale : char {
    Kelvin = 'K',
    Celsius = 'C',
    Farenheit = 'F'
};

struct Temperature {
    Scale scale;
    double value;
};

std::istream& operator>>(std::istream& in, Temperature& temperature);
bool operator<(const Temperature& lhs, const Temperature& rhs);

Temperature convert(const Temperature& from, const Scale& to);
