#include "temperature.h"

#include <cassert>

using namespace std;

int
main() {
    istringstream in_k("0K");
    Temperature k;
    in_k >> k;
    assert(k.value == 0);
    assert(k.scale == Kelvin);

    istringstream in_c("-42C");
    Temperature c;
    in_c >> c;
    assert(c.value == -42);
    assert(c.scale == Celsius);

    istringstream in_f("451F");
    Temperature f;
    in_f >> f;
    assert(f.value == 451);
    assert(f.scale == Farenheit);

    istringstream in_bad_value("zeroK");
    Temperature bad_value;
    in_bad_value >> bad_value;
    assert(in_bad_value.fail());

    istringstream in_bad_scale("0Z");
    Temperature bad_scale;
    in_bad_scale >> bad_scale;
    assert(in_bad_scale.fail());

    istringstream in_impossible("-300K");
    Temperature impossible;
    in_impossible >> impossible;
    assert(in_impossible.fail());
}
