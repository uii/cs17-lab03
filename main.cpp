#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>

#include "temperature.h"

using namespace std;

int
main() {
    size_t item_count;
    cerr << "Enter temperature count: ";
    cin >> item_count;

    cerr << "Enter temperatures: ";
    vector<Temperature> temperatures(item_count);
    for (size_t i = 0; i < item_count; i++) {
        cin >> temperatures[i];
        if (!cin) {
            cerr << "Failed to parse temperature!\n";
            return EXIT_FAILURE;
        }
    }

    size_t column_count;
    cerr << "Enter column count: ";
    cin >> column_count;

    Temperature min = temperatures[0];
    Temperature max = temperatures[0];
    for (Temperature temperature : temperatures) {
        if (temperature < min) {
            min = temperature;
        }
        if (max < temperature) {
            max = temperature;
        }
    }

    const double min_kelvins = convert(min, Kelvin).value;
    const double max_kelvins = convert(max, Kelvin).value;

    vector<size_t> counts(column_count);
    for (Temperature temperature : temperatures) {
        const double kelvins = convert(temperature, Kelvin).value;
        size_t column = (size_t)((kelvins - min_kelvins) / (max_kelvins - min_kelvins) *
                column_count);
        if (column == column_count) {
            column--;
        }
        counts[column]++;
    }

    const size_t screen_width = 80;
    const size_t axis_width = 4;
    const size_t chart_width = screen_width - axis_width;

    // Можно было бы считать в предыдущем цикле, но так более наглядно.
    size_t max_count = 0;
    for (size_t count : counts) {
        if (count > max_count) {
            max_count = count;
        }
    }
    const bool scaling_needed = max_count > chart_width;

    for (size_t count : counts) {
        if (count < 100) {
            cout << ' ';
        }
        if (count < 10) {
            cout << ' ';
        }
        cout << count << "|";

        size_t height = count;
        if (scaling_needed) {
            // Point: код должен быть в первую очередь понятным.
            const double scaling_factor = (double)chart_width / max_count;
            height = (size_t)(count * scaling_factor);
        }

        for (size_t i = 0; i < height; i++) {
            cout << '*';
        }
        cout << '\n';
    }

    return 0;
}