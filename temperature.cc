#include "temperature.h"

bool is_valid(const Temperature& temperature) {
    switch (temperature.scale) {
    case Kelvin:
        return temperature.value >= 0.0;
    case Celsius:
        return temperature.value >= -273.15;
    case Farenheit:
        return temperature.value >= -459.67;
    default:
        return false;
    }
}

std::istream&
operator>>(std::istream& in, Temperature& temperature) {
    in >> temperature.value;
    if (!in) {
        return in;
    }

    char symbol;
    in >> symbol;
    if (!in) {
        return in;
    }

    switch (symbol) {
    case 'K':
        temperature.scale = Kelvin;
        break;
    case 'C':
        temperature.scale = Celsius;
        break;
    case 'F':
        temperature.scale = Farenheit;
        break;
    default:
        in.setstate(std::ios_base::failbit);
    }

    if (!is_valid(temperature)) {
        in.setstate(std::ios_base::failbit);
    }

    return in;
}

Temperature
convert(const Temperature& from, const Scale& to) {
    if (from.scale == to) {
        return from;
    }

    double kelvins;
    switch (from.scale) {
    case Kelvin:
        kelvins = from.value;
        break;
    case Celsius:
        kelvins = from.value + 273.15;
        break;
    case Farenheit:
        kelvins = 5.0 / 9 * (from.value + 459.67);
        break;
    }

    Temperature result;
    result.scale = to;
    switch (to) {
    case Kelvin:
        result.value = kelvins;
        break;
    case Celsius:
        result.value = kelvins - 273.15;
        break;
    case Farenheit:
        result.value = kelvins * 9 / 5 - 459.67;
        break;
    }
    return result;
}

bool
operator<(const Temperature& lhs, const Temperature& rhs) {
    const Temperature converted = convert(lhs, rhs.scale);
    return converted.value < rhs.value;
}
